require_relative 'spec_helper'

describe 'map position' do

  before :each do
    @map_position = MapPosition.new(1, 3)
    @test_item = 'test item'
  end

  describe 'create new map position' do

    it 'should create map position with specified coordinates' do
      expect(@map_position.x).to eq 1
      expect(@map_position.y).to eq 3
    end

    it 'should create map position with empty list of items' do
      expect(@map_position.items.nil?).to eq false
      expect(@map_position.items.length).to eq 0
    end

    it 'should throw exception when X coordinate is negative' do
      expect { MapPosition.new(-1, 1) }.to raise_error(ArgumentError)
    end

    it 'should throw exception when Y coordinate is negative' do
      expect { MapPosition.new(1, -1) }.to raise_error(ArgumentError)
    end

    it 'should throw exception when both coordinates are negative' do
      expect { MapPosition.new(-1, -1) }.to raise_error(ArgumentError)
    end

  end

  describe 'place item in position' do

    it 'should add item to list of items in position' do
      @map_position.place_item(@test_item)
      expect(@map_position.has_item(@test_item)).to eq true
    end

    it 'should throw exception when item to place is nil' do
      expect { @map_position.place_item(nil) }.to raise_error(ArgumentError)
    end

  end

  describe 'remove item from position' do

    it 'should add item to list of items in position' do
      @map_position.place_item(@test_item)
      expect(@map_position.has_item(@test_item)).to eq true

      @map_position.remove_item(@test_item)
      expect(@map_position.has_item(@test_item)).to eq false
    end

    it 'should throw exception when item to place is nil' do
      expect { @map_position.remove_item(nil) }.to raise_error(ArgumentError)
    end

  end

  describe 'position has item' do

    it 'should find item that is placed' do
      expect(@map_position.has_item(@test_item)).to eq false
      @map_position.place_item(@test_item)
      expect(@map_position.has_item(@test_item)).to eq true
    end

    it 'should not find item that is placed then removed' do
      expect(@map_position.has_item(@test_item)).to eq false
      @map_position.place_item(@test_item)
      expect(@map_position.has_item(@test_item)).to eq true

      @map_position.remove_item(@test_item)
      expect(@map_position.has_item(@test_item)).to eq false
    end

    it 'should throw exception when item to place is nil' do
      expect { @map_position.has_item(nil) }.to raise_error(ArgumentError)
    end

  end

end