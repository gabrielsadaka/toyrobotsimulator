require_relative 'spec_helper'

describe 'robot command parser' do

  before :each do
    @robot_command_parser = RobotCommandParser.new
  end

  describe 'parse input' do

    it 'should return commands in array when there are multiple words inputted' do
      input = 'PLACE 0,0,NORTH'
      expected_commands = ['PLACE', ['0', '0', 'NORTH']]
      expect(@robot_command_parser.parse_input(input)).to eq(expected_commands)
    end

    it 'should return first word inputted when there is only one word' do
      input = 'MOVE'
      expected_commands = ['MOVE']
      expect(@robot_command_parser.parse_input(input)).to eq(expected_commands)
    end

    it 'should raise exception when input is empty' do
      input = ''
      expect { @robot_command_parser.parse_input(input) }.to raise_error(ArgumentError)
    end

    it 'should raise exception when first word is empty' do
      input = ' 0,0,NORTH'
      expect { @robot_command_parser.parse_input(input) }.to raise_error(ArgumentError)
    end

  end

  describe 'determining command inputted' do

    it 'should return PLACE command when PLACE inputted' do
      inputted_command = 'PLACE'
      expected_command = :place
      expect(@robot_command_parser.parse_command(inputted_command)).to eq(expected_command)
    end

    it 'should return MOVE command when MOVE inputted' do
      inputted_command = 'MOVE'
      expected_command = :move
      expect(@robot_command_parser.parse_command(inputted_command)).to eq(expected_command)
    end

    it 'should return LEFT command when LEFT inputted' do
      inputted_command = 'LEFT'
      expected_command = :left
      expect(@robot_command_parser.parse_command(inputted_command)).to eq(expected_command)
    end

    it 'should return RIGHT command when RIGHT inputted' do
      inputted_command = 'RIGHT'
      expected_command = :right
      expect(@robot_command_parser.parse_command(inputted_command)).to eq(expected_command)
    end

    it 'should return REPORT command when REPORT inputted' do
      inputted_command = 'REPORT'
      expected_command = :report
      expect(@robot_command_parser.parse_command(inputted_command)).to eq(expected_command)
    end

    it 'should raise exception when inputted command is blank' do
      inputted_command = ''
      expect { @robot_command_parser.parse_command(inputted_command) }.to raise_error(ArgumentError)
    end

    it 'should return invalid when inputted command is unknown' do
      inputted_command = 'RANDOM'
      expect { @robot_command_parser.parse_command(inputted_command) }.to raise_error(ArgumentError)
    end
  end

  describe 'parse PLACE command coordinates' do

    it 'should parse correctly inputted coordinates' do
      inputted_arguments = ['1', '1']
      expected_arguments = [1, 1]

      expect(@robot_command_parser.parse_coordinates(inputted_arguments)).to eq(expected_arguments)
    end

    it 'should raise exception when both inputted coordinates are blank' do
      inputted_command = ['', '']
      expect { @robot_command_parser.parse_coordinates(inputted_command) }.to raise_error(ArgumentError)
    end

    it 'should raise exception when either inputted coordinates are blank' do
      inputted_command = ['1', '']
      expect { @robot_command_parser.parse_coordinates(inputted_command) }.to raise_error(ArgumentError)
    end

    it 'should raise exception when both inputted coordinates are negative' do
      inputted_command = ['-1', '-1']
      expect { @robot_command_parser.parse_coordinates(inputted_command) }.to raise_error(ArgumentError)
    end

    it 'should raise exception when either inputted coordinates are negative' do
      inputted_command = ['1', '-1']
      expect { @robot_command_parser.parse_coordinates(inputted_command) }.to raise_error(ArgumentError)
    end

    it 'should raise exception when both inputted coordinates are not numbers' do
      inputted_command = ['not', 'number']
      expect { @robot_command_parser.parse_coordinates(inputted_command) }.to raise_error(ArgumentError)
    end

    it 'should raise exception when either inputted coordinates are not numbers' do
      inputted_command = ['a', '1']
      expect { @robot_command_parser.parse_coordinates(inputted_command) }.to raise_error(ArgumentError)
    end

  end

  describe 'parse PLACE command facing' do

    it 'should parse north facing' do
      inputted_arguments = 'NORTH'
      expected_arguments = :north

      expect(@robot_command_parser.parse_facing(inputted_arguments)).to eq(expected_arguments)
    end

    it 'should parse south facing' do
      inputted_arguments = 'SOUTH'
      expected_arguments = :south

      expect(@robot_command_parser.parse_facing(inputted_arguments)).to eq(expected_arguments)
    end

    it 'should parse east facing' do
      inputted_arguments = 'EAST'
      expected_arguments = :east

      expect(@robot_command_parser.parse_facing(inputted_arguments)).to eq(expected_arguments)
    end

    it 'should parse west facing' do
      inputted_arguments = 'WEST'
      expected_arguments = :west

      expect(@robot_command_parser.parse_facing(inputted_arguments)).to eq(expected_arguments)
    end

    it 'should raise exception when inputted facing is blank' do
      inputted_command = ''
      expect { @robot_command_parser.parse_facing(inputted_command) }.to raise_error(ArgumentError)
    end

    it 'should return invalid when inputted facing is unknown' do
      inputted_command = 'RANDOM'
      expect { @robot_command_parser.parse_facing(inputted_command) }.to raise_error(ArgumentError)
    end
  end

  describe 'process input' do

    it 'should process commands and arguments for PLACE command correctly' do
      input = 'PLACE 0,0,NORTH'
      expected_commands = [:place, [[0, 0], :north]]
      expect(@robot_command_parser.process_input_line(input)).to eq(expected_commands)
    end

    it 'should process MOVE command correctly' do
      input = 'MOVE'
      expected_commands = [:move]
      expect(@robot_command_parser.process_input_line(input)).to eq(expected_commands)
    end

    it 'should process LEFT command correctly' do
      input = 'LEFT'
      expected_commands = [:left]
      expect(@robot_command_parser.process_input_line(input)).to eq(expected_commands)
    end

    it 'should process RIGHT command correctly' do
      input = 'RIGHT'
      expected_commands = [:right]
      expect(@robot_command_parser.process_input_line(input)).to eq(expected_commands)
    end

    it 'should process REPORT command correctly' do
      input = 'REPORT'
      expected_commands = [:report]
      expect(@robot_command_parser.process_input_line(input)).to eq(expected_commands)
    end

  end
end
