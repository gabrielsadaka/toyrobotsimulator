require_relative 'spec_helper'

describe 'robot' do

  before :each do
    @simulator_map = SimulatorMap.new(5, 5)
    @robot = Robot.new(@simulator_map)
  end

  describe 'place robot on map position' do

    it 'should place robot on new position within map' do
      @robot.place_on_position(1, 1, :north)
      expect(@robot.find_current_position).to eq @simulator_map.get_position(1, 1)
    end

    it 'should place robot on new position on border of map' do
      @robot.place_on_position(4, 4, :north)
      expect(@robot.find_current_position).to eq @simulator_map.get_position(4, 4)
    end

    it 'should set the facing correctly' do
      @robot.place_on_position(4, 4, :south)
      expect(@robot.facing).to eq :south
    end

    it 'should raise exception when facing is invalid' do
      expect { @robot.place_on_position(4, 4, :test_error) }.to raise_error(ArgumentError)
    end

  end

  describe 'move robot in the direction it is facing' do

    it 'should move the robot up one square when facing north' do
      @robot.place_on_position(1, 1, :north)
      @robot.move
      expect(@robot.find_current_position).to eq @simulator_map.get_position(1, 2)
    end

    it 'should move the robot down one square when facing south' do
      @robot.place_on_position(1, 1, :south)
      @robot.move
      expect(@robot.find_current_position).to eq @simulator_map.get_position(1, 0)
    end

    it 'should move the robot right one square when facing east' do
      @robot.place_on_position(1, 1, :east)
      @robot.move
      expect(@robot.find_current_position).to eq @simulator_map.get_position(2, 1)
    end

    it 'should move the robot left one square when facing west' do
      @robot.place_on_position(1, 1, :west)
      @robot.move
      expect(@robot.find_current_position).to eq @simulator_map.get_position(0, 1)
    end

    it 'should raise exception when moving in invalid direction' do
      @robot.instance_variable_set(:@facing, :invalid)
      expect { @robot.move }.to raise_error(RuntimeError)
    end

  end

  describe 'rotate robot left' do

    it 'should face the robot west when rotating left from north' do
      @robot.place_on_position(1, 1, :north)
      @robot.rotate_left
      expect(@robot.facing).to eq :west
    end

    it 'should face the robot south when rotating left from west' do
      @robot.place_on_position(1, 1, :west)
      @robot.rotate_left
      expect(@robot.facing).to eq :south
    end

    it 'should face the robot east when rotating left from south' do
      @robot.place_on_position(1, 1, :south)
      @robot.rotate_left
      expect(@robot.facing).to eq :east
    end

    it 'should face the robot north when rotating left from east' do
      @robot.place_on_position(1, 1, :east)
      @robot.rotate_left
      expect(@robot.facing).to eq :north
    end

    it 'should raise exception when rotating from invalid direction' do
      @robot.instance_variable_set(:@facing, :invalid)
      expect { @robot.rotate_left }.to raise_error(RuntimeError)
    end

  end

  describe 'rotate robot right' do

    it 'should face the robot east when rotating right from north' do
      @robot.place_on_position(1, 1, :north)
      @robot.rotate_right
      expect(@robot.facing).to eq :east
    end

    it 'should face the robot north when rotating right from west' do
      @robot.place_on_position(1, 1, :west)
      @robot.rotate_right
      expect(@robot.facing).to eq :north
    end

    it 'should face the robot east when rotating right from south' do
      @robot.place_on_position(1, 1, :south)
      @robot.rotate_right
      expect(@robot.facing).to eq :west
    end

    it 'should face the robot north when rotating right from east' do
      @robot.place_on_position(1, 1, :east)
      @robot.rotate_right
      expect(@robot.facing).to eq :south
    end

    it 'should raise exception when rotating from invalid direction' do
      @robot.instance_variable_set(:@facing, :invalid)
      expect { @robot.rotate_right }.to raise_error(RuntimeError)
    end

  end

  describe 'report on map position and facing' do

    it 'should report empty string if robot is not on map' do
      expect(@robot.report_position).to eq nil
    end

    it 'should report on map position and facing' do
      @robot.place_on_position(1, 1, :north)
      expect(@robot.report_position).to eq '1,1,north'
    end

  end

end