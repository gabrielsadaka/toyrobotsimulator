require_relative 'spec_helper'

describe 'simulator map' do

  before :each do
    @test_item = 'test_robot'
    @simulatorMap = SimulatorMap.new(5, 5)
  end

  describe 'map creation' do

    it 'should set the correct dimensions' do
      expect(@simulatorMap.width).to eq 5
      expect(@simulatorMap.height).to eq 5
    end

    it 'should create the correct number of rows' do
      expect(@simulatorMap.rows.length).to eq 5
    end

    it 'should create the correct number of columns' do
      @simulatorMap.rows.each { |row|
        expect(row.length).to eq 5
      }
    end

  end

  describe 'get position on map' do

    it 'should retrieve the correct position based on the coordinates' do
      position = @simulatorMap.get_position(1, 3)
      expect(position.x).to eq 1
      expect(position.y).to eq 3
    end

  end

  describe 'validate new item map position' do

    it 'should throw exception when position has negative X coordinate' do
      expect { @simulatorMap.validate_position(-1, 1) }.to raise_error(ArgumentError)
    end

    it 'should throw exception when position has negative Y coordinate' do
      expect { @simulatorMap.validate_position(1, -1) }.to raise_error(ArgumentError)
    end

    it 'should throw exception when position has negative coordinates' do
      expect { @simulatorMap.validate_position(-1, -1) }.to raise_error(ArgumentError)
    end

    it 'should throw exception when X coordinate is outside of map' do
      expect { @simulatorMap.validate_position(6, 1) }.to raise_error(ArgumentError)
    end

    it 'should throw exception when Y coordinate is outside of map' do
      expect { @simulatorMap.validate_position(1, 6) }.to raise_error(ArgumentError)
    end

    it 'should throw exception when both coordinates are outside of map' do
      expect { @simulatorMap.validate_position(6, 6) }.to raise_error(ArgumentError)
    end

  end

  describe 'find item on map' do

    it 'should return nil for an item not on the map' do
      expect(@simulatorMap.find_item(@test_item)).to eq nil
    end

    it 'should return nil for an item not on the map' do
      expected_position = @simulatorMap.get_position(1, 1)
      @simulatorMap.place_item(@test_item, 1, 1)
      expect(@simulatorMap.find_item(@test_item)).to eq expected_position
    end

    it 'should throw exception item is nil' do
      expect { @simulatorMap.find_item(nil) }.to raise_error(ArgumentError)
    end

  end

  describe 'place item on map position' do

    it 'should throw exception when item is nil' do
      expect { @simulatorMap.place_item(nil, 1, 1) }.to raise_error(ArgumentError)
    end

    it 'should place a new item on the map' do
      expected_position = @simulatorMap.get_position(1, 1)
      expect(expected_position.has_item(@test_item)).to eq false

      @simulatorMap.place_item(@test_item, 1, 1)
      expect(expected_position.has_item(@test_item)).to eq true
    end

    it 'should move an existing item from one position to another on the map' do
      expected_position = @simulatorMap.get_position(1, 1)
      @simulatorMap.place_item(@test_item, 1, 1)
      expect(expected_position.has_item(@test_item)).to eq true

      expected_new_position = @simulatorMap.get_position(1, 3)
      @simulatorMap.place_item(@test_item, 1, 3)
      expect(expected_new_position.has_item(@test_item)).to eq true
    end

    it 'should place a new item on the origin' do
      expected_position = @simulatorMap.get_position(0, 0)
      expect(expected_position.has_item(@test_item)).to eq false

      @simulatorMap.place_item(@test_item, 0, 0)
      expect(expected_position.has_item(@test_item)).to eq true
    end

    it 'should place a new item on the border' do
      expected_position = @simulatorMap.get_position(4, 4)
      expect(expected_position.has_item(@test_item)).to eq false

      @simulatorMap.place_item(@test_item, 4, 4)
      expect(expected_position.has_item(@test_item)).to eq true
    end

  end

end