module ToyRobotSimulator

  # @author Gabriel Sadaka
  # This class parses the commands inputted
  # to the application to move and control the toy robot simulator.
  #
  class RobotCommandParser

    # Processes a line of input from the application.
    #
    # == Parameters:
    # input::
    #   Line of input.
    #
    # == Returns:
    # Array with the command and if specified arguments in the input.
    #
    def process_input_line(input)
      parsed_input = parse_input(input)

      command = parse_command(parsed_input[0])

      if parsed_input.length > 1
        arguments = parsed_input[1]
        facing = parse_facing(arguments[2])
        coordinates = parse_coordinates(arguments[0..1])
        [command, [coordinates, facing]]
      else
        [command]
      end
    end

    # Parses a line of input from the application.
    # Splitting the command from the arguments.
    #
    # == Parameters:
    # input::
    #   Line of input.
    #
    # == Returns:
    # Array with the command string and if specified arguments string in the input.
    #
    def parse_input(input)
      raise(ArgumentError, ':input must not be empty') if (input.length == 0)
      raise(ArgumentError, 'first word of :input must not be empty') if (input[0] == ' ')
      words_inputted = input.split(' ')
      command = words_inputted[0]
      if words_inputted.length == 2
        arguments = words_inputted[1].split(',')
        [command, arguments]
      else
        [command]
      end
    end

    # Parses the command specified in the input.
    #
    # == Parameters:
    # command::
    #   Inputted Command.
    #
    # == Returns:
    # Symbol representing the command inputted.
    #
    def parse_command(command)
      case command
        when 'PLACE'
          parsed_command = :place
        when 'MOVE'
          parsed_command = :move
        when 'LEFT'
          parsed_command = :left
        when 'RIGHT'
          parsed_command = :right
        when 'REPORT'
          parsed_command = :report
        else
          raise(ArgumentError, ':command is invalid')
      end
      parsed_command
    end

    # Parses facing specified in the input.
    #
    # == Parameters:
    # facing::
    #   Inputted Facing.
    #
    # == Returns:
    # Symbol representing the facing inputted.
    #
    def parse_facing(facing)
      case facing
        when 'NORTH'
          parsed_facing = :north
        when 'SOUTH'
          parsed_facing = :south
        when 'EAST'
          parsed_facing = :east
        when 'WEST'
          parsed_facing = :west
        else
          raise(ArgumentError, 'facing is invalid')
      end
      parsed_facing
    end

    # Parses coordinates specified in the input.
    #
    # == Parameters:
    # inputted_arguments::
    #   Arguments inputted into application.
    #
    # == Returns:
    # Array of integers representing coordinates in input.
    #
    def parse_coordinates(inputted_arguments)
      x = inputted_arguments[0]
      y = inputted_arguments[1]

      x.match(/\d+/) ? parsed_x = Integer(x) : raise(ArgumentError, 'X coordinate has to be a number')

      y.match(/\d+/) ? parsed_y = Integer(y) : raise(ArgumentError, 'Y coordinate has to be a number')

      if parsed_x < 0
        raise(ArgumentError, 'X coordinate has to be greater than 0')
      end

      if parsed_y < 0
        raise(ArgumentError, 'X coordinate has to be greater than 0')
      end

      [parsed_x, parsed_y]
    end

  end
end