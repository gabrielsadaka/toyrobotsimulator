module ToyRobotSimulator

  # @author Gabriel Sadaka
  # This class holds the details about the Robot
  # and controls the movements of it.
  class Robot
    attr_reader :map, :facing

    # Initializes a new robot in a specific map.
    #
    # == Parameters:
    # map::
    #   Map to place robot in.
    #
    # == Returns:
    # A new Robot
    #
    def initialize(map)
      @map = map

      @facings = [
          :north,
          :east,
          :south,
          :west
      ]
    end

    # Places the robot on the specified map position facing in
    # the specified direction.
    #
    # == Parameters:
    # x::
    #   X Coordinate of position.
    #
    # y::
    #   Y Coordinate of position.
    #
    # facing::
    #   Direction for robot to face.
    #
    def place_on_position(x, y, facing)
      @map.place_item(self, x, y)

      case facing
        when :north, :south, :east, :west
          @facing = facing
        else
          raise(ArgumentError, ':facing is not a valid compass direction')
      end

    end

    # Finds the current position of the robot on the map.
    #
    # == Returns:
    # The position of the robot on the map.
    #
    def find_current_position
      @map.find_item(self)
    end

    # Moves the robot in the direction it is currently facing.
    #
    def move
      current_position = find_current_position
      case @facing
        when :north
          place_on_position(current_position.x, current_position.y + 1, @facing)
        when :south
          place_on_position(current_position.x, current_position.y - 1, @facing)
        when :east
          place_on_position(current_position.x + 1, current_position.y, @facing)
        when :west
          place_on_position(current_position.x - 1, current_position.y, @facing)
        else
          raise 'Robot is not facing in a valid direction, cannot move.'
      end
    end

    # Rotates the robot left of the direction it is currently facing.
    #
    def rotate_left
      # Rotate left decrements index
      current_facing_index = get_current_facing_index

      if current_facing_index != 0
        new_facing_index = current_facing_index - 1
      else
        new_facing_index = @facings.length - 1
      end

      @facing = @facings[new_facing_index]
    end

    # Rotates the robot right of the direction it is currently facing.
    #
    def rotate_right
      # Rotate right increments index
      current_facing_index = get_current_facing_index

      if current_facing_index != @facings.length - 1
        new_facing_index = current_facing_index + 1
      else
        new_facing_index = 0
      end

      @facing = @facings[new_facing_index]
    end

    # Finds the index of the direction the robot is currently facing in the facing array.
    #
    # == Returns:
    # Index of robot facing.
    #
    def get_current_facing_index
      current_facing_index = @facings.index(@facing)

      if current_facing_index.nil?
        raise 'Robot is not facing in a valid direction, cannot rotate.'
      end

      current_facing_index
    end

    # Returns a formatted string of the robots current position and direction.
    #
    def report_position
      current_position = find_current_position

      if current_position.nil?
        nil
      else
        "#{current_position.x},#{current_position.y},#{@facing}"
      end

    end

  end

end