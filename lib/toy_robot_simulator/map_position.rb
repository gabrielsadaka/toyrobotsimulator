module ToyRobotSimulator

  # @author Gabriel Sadaka
  # This class holds the details about and controls the individual position on
  # the simulator map
  class MapPosition
    attr_reader :x, :y, :items

    # Initializes position with the x and y coordinates specified.
    #
    # == Parameters:
    # x::
    #   X Coordinate of position.
    #
    # y::
    #   Y Coordinate of position.
    #
    # == Returns:
    # A new Map Position
    #
    def initialize(x, y)
      raise(ArgumentError, ':x must positive') if x < 0
      raise(ArgumentError, ':y must be positive') if y < 0
      @x = x
      @y = y
      @items = []
    end

    # Adds an item to the position.
    #
    # == Parameters:
    # item::
    #   Item to add.
    #
    def place_item(item)
      raise(ArgumentError, ':item must not be nil') if item.nil?
      @items.push(item)
    end

    # Removes an item from the position.
    #
    # == Parameters:
    # item::
    #   Item to remove.
    #
    def remove_item(item)
      raise(ArgumentError, ':item must not be nil') if item.nil?
      @items.delete(item)
    end

    # Checks if the item is on the position.
    #
    # == Parameters:
    # item::
    #   Item to look for.
    #
    # == Returns:
    # true, if item exists on the position.
    # false, if the item does not exist on the position.
    #
    def has_item(item)
      raise(ArgumentError, ':item must not be nil') if item.nil?

      !@items.index(item).nil?
    end

  end

end
