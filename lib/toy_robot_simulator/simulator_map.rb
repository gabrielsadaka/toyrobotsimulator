module ToyRobotSimulator

  # @author Gabriel Sadaka
  # This class holds the details about the simulators map
  # and controls the placement of items across it
  class SimulatorMap
    attr_reader :width, :height, :rows

    # Initializes the map with a grid of the specified
    # width and height
    #
    # == Parameters:
    # width::
    #   The number of columns in the grid.
    #
    # height::
    #   The number of rows in the grid.
    #
    # == Returns:
    # A new Simulator Map
    #
    def initialize(width, height)
      @width = width
      @height = height
      # Stores the rows and columns in a 2d array
      @rows = []
      (0...width).each { |x|
        x_positions = []
        (0...height).each { |y|
          x_positions.push(MapPosition.new(x, y))
        }
        @rows.push(x_positions)
      }
    end

    # Determines whether the specified coordinates are valid on the map.
    #
    # == Parameters:
    # x::
    #   X Coordinate to check.
    #
    # y::
    #   Y Coordinate to check.
    #
    def validate_position(x, y)
      raise(ArgumentError, ':x must be within range of map') if x > @width || x < 0
      raise(ArgumentError, ':y must be within range of map') if y > @height || y < 0
    end

    # Gets the specified position on the map.
    #
    # == Parameters:
    # x::
    #   X Coordinate to retrieve.
    #
    # y::
    #   Y Coordinate to retrieve.
    #
    def get_position(x, y)
      validate_position(x, y)
      @rows[x][y]
    end

    # Retrieves the position of the specified item on the map if is on there.
    #
    # == Parameters:
    # item::
    #   Item to look for.
    #
    # == Returns:
    # The position of the item if found, nil if it is not on the map.
    #
    def find_item(item)
      if item.nil?
        raise(ArgumentError, ':item must not be nil')
      end
      @rows.each { |current_row|
        (current_row).each { |current_position|
          if current_position.has_item(item)
            return current_position
          end
        }
      }
      nil
    end

    # Moves the item from it's current position to the newly specified position
    # if it is currently on the map, places it on the new position if it is not
    # already on the map.
    #
    # == Parameters:
    # item::
    #   Item to place.
    #
    # x::
    #   X Coordinate of position to place on.
    #
    # y::
    #   Y Coordinate of position to place on.
    #
    def place_item(item, x, y)
      validate_position(x, y)
      if item.nil?
        raise(ArgumentError, ':item must not be nil')
      end

      # find current position
      current_position = find_item(item)

      # remove from current position if currently exists on map
      unless current_position.nil?
        current_position.remove_item(item)
      end

      # find new position
      new_position = get_position(x, y)

      # add to new position
      new_position.place_item(item)
    end

  end

end
