require_relative 'toy_robot_simulator'
include ToyRobotSimulator

# Reads in all of the standard input
stdin_input = $stdin.read

# Initialise application simulation variables
robot_command_parser = RobotCommandParser.new
simulator_map = SimulatorMap.new(5, 5)
robot = Robot.new(simulator_map)

# Process each line from standard input, delegating it to the parser
# then to the respective robot methods depending on command inputted in each line.
stdin_input.each_line do |line|
  begin
    line_input = robot_command_parser.process_input_line(line)
    case line_input[0]
      when :place
        arguments = line_input[1]
        robot.place_on_position(arguments[0][0], arguments[0][1], arguments[1])
      when :move
        robot.move
      when :left
        robot.rotate_left
      when :right
        robot.rotate_right
      when :report
        position = robot.report_position
        unless position.nil?
          puts position
        end
      else
        #Ignore invalid command
    end
  rescue Exception
    # Exception will be raised by parser when invalid input is detected however according to spec, invalid input
    # must be ignored.
  end
end