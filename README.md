# Toy Robot Simulator By Gabriel Sadaka #

This repository contains an implementation of the Toy Robot Simulator Coding Challenge written by Gabriel Sadaka in Ruby using TDD. 

The application is written using pure Ruby with no dependencies and uses RSpec for unit testing. 

Documented using Ruby yard documentation style.

The application has been tested on the following platforms:

* Windows 10 x64 Ruby 2.3.0
* Windows 10 x64 Bash for Ubuntu Ruby 1.9.3
* Ubuntu Server 16.04 x64 Ruby 2.3.0

To run the application please use the following commands on a Unix/Linux based system:
```
#!bash
git clone https://bitbucket.org/gabrielsadaka/toyrobotsimulator.git
cd toyrobotsimulator/lib
cat test.txt | ruby application.rb
```

To run the application please use the following commands on a Windows based system:
```
#!bash
git clone https://bitbucket.org/gabrielsadaka/toyrobotsimulator.git
cd toyrobotsimulator\lib
ruby application.rb < test.txt
```

To run the unit tests please use the following commands on a Unix/Linux based system:
```
#!bash
git clone https://bitbucket.org/gabrielsadaka/toyrobotsimulator.git
gem install rspec
rspec --pattern spec\**\*_spec.rb
```

To run the unit tests please use the following commands on a Windows based system:
```
#!bash
git clone https://bitbucket.org/gabrielsadaka/toyrobotsimulator.git
gem install rspec
rspec --pattern spec/**/*_spec.rb
```